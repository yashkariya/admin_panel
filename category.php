<?php
include_once("header.php");
?>
<div class="container">
    <legend><strong><center>Category List</center></strong></legend>
    <div><button type="button" class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#addCategory">Add Category <i class="fa fa-plus" aria-hidden="true"></i></button></div><br><br><br>
    <table id="categoryDatatable" class="table table-striped table-hover responsive" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Category Name</th>
                <th>Category Image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Category Name</th>
                <th>Category Image</th>
                <th>Action</th>
            </tr>
        </tfoot>

        <tbody>

        </tbody>
    </table>
</div>
<div id="addCategory" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <form id="addCategoryForm" class="form-horizontal" data-toggle="validator" role="form">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title" id="modalTitle">
                    Add Category
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-sm-4">Category Name : </label>
                    <div class="col-sm-8">
                        <input type="text" name="categoryName" id="categoryName" class="form-control" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Category Image : </label>
                    <div class="col-sm-8">
                        <input type="file" name="categoryImage" id="categoryImage" required>
                        <div class="help-block with-errors"></div>
                        <div id="fileerror"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Add</button>
            </div>
        </form>
    </div>

  </div>
</div>

<div id="editCategory" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <form id="editCategoryForm" class="form-horizontal" role="form" data-toggle="validator">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title" id="modalTitle">
                    Edit Category
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="hidden_image" id="hidden_image">
                <input type="hidden" name="hidden_id" id="hidden_id">
                <div class="form-group">
                    <label class="control-label col-sm-4">Category Name : </label>
                    <div class="col-sm-8">
                        <input type="text" name="editcategoryName" id="editcategoryName" class="form-control" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Category Image : </label>
                    <div class="col-sm-8">
                        <input type="file" name="editcategoryImage" id="editcategoryImage" >
                        <div class="help-block with-errors"></div>
                        <div id="fileediterror"></div>
                        <img src="" height="50" width="50" id="editImage">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Save Changes</button>
            </div>
        </form>
    </div>

  </div>
</div>
<?php include_once('js_files.php'); ?>
<script type="text/javascript">
$(document).ready(function() {
   var table = $('#categoryDatatable').DataTable({
            "ajax": {
                "url": "fetch_category.php",
                "processing": true,
                "serverSide": true,
            },  
            'columnDefs': [
            {
                'targets': 3,
                'searchable':false,
                'orderable':false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
            
                    return '<a href="javascript:editCategory('+data+')" class="btn btn-primary btn-flat"><i class="fa fa-pencil" title="Edit Category"></i></a> <a href="javascript:deleteCategory('+data+')" class="btn btn-danger btn-flat"><i class="fa fa-trash" title="Delete Category"></i></a>';
                }
            },
            {
                'targets': 2,
                'searchable':false,
                'orderable':false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
            
                    return '<img src="<?php echo IMG_URL?>'+data+'" width="50" height="50">';
                }
            }],
            'order': [1, 'asc'],
            "createdRow": function(row, data, dataIndex){
                $(row).attr("id", "tblRow_" + data[0]);
            }
        }); 
});

function deleteCategory(id)
{
    console.log(id);
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Category!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function (isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url: "<?php echo BASE_URL.'classes/methods_api.php'?>",
            type: "POST",
            data: {
                category_id: id,
                action : "categoryDelete"
            },
            dataType: "json",
            success: function (data) {
                swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                    location.reload();
                },2000);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error");
            }
        });
    });
}
$("#categoryImage").change(function () {
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
    $("#fileerror").html('<p style="color:red">Only formats are allowed : '+fileExtension.join(', ')+'</p>');
    }
});
$("#editcategoryImage").change(function () {
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
    $("#fileediterror").html('<p style="color:red">Only formats are allowed : '+fileExtension.join(', ')+'</p>');
    }
});
var file=null;
var editFile = null;
var udata = [];
$('#categoryImage').change(function()
{
    file = this.files[0];
});
$('#editcategoryImage').change(function()
{
    editFile = this.files[0];
});
$('#addCategoryForm').validator().on('submit', function(e){
    if (!e.isDefaultPrevented()) {
        frm = new FormData(addCategoryForm);
        frm.append("file",file);
        frm.append("action","addCategory");
        sa_ajax(frm, save_success, save_fail);
        e.preventDefault();
    }
});
function save_success(res)
{
    if(res['status_code'] == 200)
    {
        sa_message("success",res['message']);
        $("#addCategory").modal('hide');
        reset_form("addCategoryForm");
        $("#categoryDatatable").DataTable().ajax.reload();
    }

    if(res['status_code'] == 500) 
    {
        sa_message("error",res['message']);
    }
}

function save_fail(err)
{
    console.log(err);
}
function reset_form(form_id)
{
    document.getElementById(form_id).reset();
}

function editCategory(id)
{
    console.log(id);
    frm = new FormData();
    frm.append('action','categoryById');
    frm.append('category_id',id);
    sa_ajax(frm, get_success, get_fail); 
    function get_success(res)
    {
        console.log(res);
        udata = res['data'][0];
        $("#editCategory").modal('show');
        $("#editcategoryName").val(udata['category_name']);
        $("#editImage").attr('src',"<?= IMG_URL ?>"+udata['category_image']);
        $("#hidden_image").val(udata['category_image']);
        $("#hidden_id").val(udata['id']);
    }
    function get_fail(res)
    {
        console.log(res);
    }
}

$("#editCategoryForm").validator().on("submit",function(e)
{   
    if(!e.isDefaultPrevented())
    {
        frm = new FormData(editCategoryForm);
        frm.append('file',editFile);
        frm.append('action',"editCategory");
        sa_ajax(frm, edit_success, edit_fail); 
        e.preventDefault();
    }

    function edit_success(res)
    {
        console.log(res);
        if(res['status_code'] == 200)
        {
            sa_message("success",res['message']);
            $("#editCategory").modal('hide');
            $("#categoryDatatable").DataTable().ajax.reload();
            $("#editcategoryImage").val('');
            // $("#editId").attr("value",'');
        }

        if(res['status_code'] == 500) 
        {
            sa_message("error",res['message']);
        }
    }

    function edit_fail(err)
    {
        sa_message("error",err['message']);
    }
});
</script>
<style type="text/css">
.nav-tabs > li {
    position:relative;
}
.nav-tabs > li > a {
    display:inline-block;
}
.nav-tabs > li > span {
    display:none;
    cursor:pointer;
    position:absolute;
    right: 6px;
    top: 8px;
    color: red;
}
.nav-tabs > li:hover > span {
    display: inline-block;
}
</style>

