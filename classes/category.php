<?php
	@session_start();
	@include_once('../config.php');
	class category
	{
		function addCategory($db,$data)
		{
			if(!empty($data['file']))
			{
	            //$ext = end(explode(".", $_FILES['fileGig']['name']));
	            $tmp = explode('.', $data['file']['name']);
				$ext = end($tmp);
	        	
	        	$var = $data['file'];        	

	        	$new_file_name = "original-".time().'.'.$ext;
	        	move_uploaded_file($data['file']['tmp_name'], UPLOADS.$new_file_name);
	        	$filedata = new stdClass();
				$filedata->status_code = 200;
				$filedata->file_path = IMG_URL.$new_file_name;
			}
	        else
	        {
	            $file = '';
	        }
	            
	        $data['category_image'] = $new_file_name;
			$insert_query = 
			"INSERT INTO category 
			(`category_name`, `category_image`)
			VALUES
			(
			'".$data['categoryName']."',
			'".$data['category_image']."'
			)";
	
			$insert = $db->query($insert_query);

			if($insert > 0 ) 
			{
		  		$response = new stdClass();
				$response->status_code = 200;
				$response->message = "Category added successfully.!";
				echo json_encode($response);
			}
			else
			{
				$response = new stdClass();
				$response->status_code = 500;
				$response->message = "Internal server error.";
				echo json_encode($response);
			}
		}
		function deleteCategory($db,$data)
		{
			$query = "update category set status = 3 where id = ".$data['category_id'];
			$delete = $db->query($query);
				 
			if($delete) 
			{
		  		$response = new stdClass();
				$response->status_code = 200;
				$response->message = "Category deleted successfully.!";
				echo json_encode($response);
			}
			else
			{
				$response = new stdClass();
				$response->status_code = 500;
				$response->message = "Internal server error.";
				echo json_encode($response);
			}
		}
		function getCategoryById($db,$data)
		{
			$query = "select * from category where id = ".$data['category_id'];
			$get = $db->query($query);

			if($get) 
			{
		  		$response = new stdClass();
				$response->status_code = 200;
				$response->message = "Category get successfully.!";
				$response->data = $get;
				echo json_encode($response);
			}
			else
			{
				$response = new stdClass();
				$response->status_code = 500;
				$response->message = "Internal server error.";
				echo json_encode($response);
			}
		}
		function getCategory($db)
		{
			$query = "select * from category where status = 1";
			$get = $db->query($query);
			return $get;
		}
		function editCategory($db,$data)
		{
			$new_file_name = "";
			if(!empty($data['file']['name']))
			{
	            //$ext = end(explode(".", $_FILES['fileGig']['name']));
	            $tmp = explode('.', $data['file']['name']);
				$ext = end($tmp);
	        	
	        	$var = $data['file'];        	

	        	$new_file_name = "original-".time().'.'.$ext;
	        	move_uploaded_file($data['file']['tmp_name'], UPLOADS.$new_file_name);
	        	$filedata = new stdClass();
				$filedata->status_code = 200;
				$filedata->file_path = IMG_URL.$new_file_name;
			}
	        else
	        {
	            $new_file_name = $data['hidden_image'];
	        }	
	        $data['category_image'] = $new_file_name;
			$query = "update category set category_name = '".$data['editcategoryName']."', category_image = '".$data['category_image']."' where id = ".$data['hidden_id'];
			$update = $db->query($query);

			
			if($update) 
			{
		  		$response = new stdClass();
				$response->status_code = 200;
				$response->message = "Category updated successfully.!";
				echo json_encode($response);
			}
			else
			{
				$response = new stdClass();
				$response->status_code = 500;
				$response->message = "Trying to update same value.!";
				echo json_encode($response);
			}
		}
	}
?>