<?php
	@session_start();
	@include_once('../config.php');
	class channel
	{
		public function fetchChannelData($db,$data)
		{
			//Get videos from channel by YouTube Data API
			$API_key    = 'AIzaSyAzvNFgKdL6Kh7kQgjCRwXOI5JO8ULwLeA';
			$channelID  = $data['channelId'];
			$maxResults = 10;

			$videoList = json_decode(@file_get_contents('https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='.$channelID.'&maxResults='.$maxResults.'&key='.$API_key.''));

			if( $videoList ) {
				$response = new stdClass();
				$response->status_code = 200;
				$response->message = "success";
				$response->data = $videoList;
			}
			else
			{
				$response = new stdClass();
				$response->status_code = 500;
				$response->message = "Channel not found";
			}
			echo json_encode($response);

		}

		public function addChannelData($db,$data)
		{
			$insert_query = 
				"INSERT INTO channel 
				(`category_id`, `channel_id`, `channel_name`, `channel_image`)
				VALUES
				(
				".$data['category_id'].",
				'".$data['channelId']."',
				'".$data['channelName']."',
				'".$data['channel_image']."'
				)";

			$insert = $db->query($insert_query);
	

			if($insert > 0 ) 
			{
		  		$response = new stdClass();
				$response->status_code = 200;
				$response->message = "Channel added successfully.!";
				echo json_encode($response);
			}
			else
			{
				$response = new stdClass();
				$response->status_code = 500;
				$response->message = "Internal server error.";
				echo json_encode($response);
			}
		}
		function getChannelById($db,$data)
		{
			$query = "select * from channel where id = ".$data['channel_id'];
			$get = $db->query($query);

			if($get) 
			{
		  		$response = new stdClass();
				$response->status_code = 200;
				$response->message = "Channel get successfully.!";
				$response->data = $get;
				echo json_encode($response);
			}
			else
			{
				$response = new stdClass();
				$response->status_code = 500;
				$response->message = "Internal server error.";
				echo json_encode($response);
			}
		}
	}
?>