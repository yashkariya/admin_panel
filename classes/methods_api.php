<?php
	include_once("Db.class.php");
	include_once("users.php");
	include_once("category.php");
	include_once("channel.php");
	$db = new Db();
	if(isset($_POST['action']))
	{
		if($_POST['action']=="signin")
		{
			$user = new User();
			$user->login($db,$_POST);
		}

		if($_POST['action']=="categoryDelete")
		{
			$category = new Category();
			$category->deleteCategory($db,$_POST);
		}

		if($_POST['action'] == "addCategory")
		{
			$data = $_POST;
			$data['file'] = $_FILES['categoryImage'];
			$category = new Category();
			$category->addCategory($db,$data);
		}
 		if($_POST['action'] == "categoryById")
		{
			$category = new Category();
			$category->getCategoryById($db,$_POST);
		}
		if($_POST['action'] == "editCategory")
		{
			$data = $_POST;
			$data['file'] = $_FILES['editcategoryImage'];
			$category = new Category();
			$category->editCategory($db,$data);
		}
		if($_POST['action'] == "fetchChannelData")
		{
			$channel = new Channel();
			$channel->fetchChannelData($db,$_POST);
		}
		if($_POST['action'] == "addChannel")
		{
			$channel = new Channel();
			$channel->addChannelData($db,$_POST);
		}
		if($_POST['action'] == "channelById")
		{
			$category = new Channel();
			$category->getChannelById($db,$_POST);
		}

	}
?>