<?php
	define("BASE_URL","http://localhost/admin_panel/");
	define("IMG_URL",BASE_URL."img/uploads/");
	define("UPLOADS",$_SERVER["DOCUMENT_ROOT"]."/admin_panel/img/uploads/");

	include_once("classes/Db.class.php");
	include_once("classes/users.php");
	include_once("classes/category.php");

	function is_login()
	{
		$db = new Db();
		$userObj = new User();	
		if($userObj->is_login($db))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function get_category()
	{
		if(is_login())
		{
			$db = new Db();
			$categoryObj = new Category();	
			$category = $categoryObj->getCategory($db);
			return $category;
		}
	}
	

?>