<?php
include_once("classes/Db.class.php");
include_once("classes/users.php");

$table = <<<EOT
 (
    SELECT 
    c.id as category_id,
    c.category_name,
    ch.channel_name,
    ch.id as id,
    ch.channel_image,
    ch.channel_id
    FROM channel ch
    JOIN category c ON c.id = ch.category_id
 ) temp
EOT;
//$table = 'channel';
$primaryKey = 'id';
// $where = "status = 1";
$columns = array(
    array( 'db' => 'id', 'dt' => 0),
    array( 'db' => 'category_name',   'dt' => 1 ),
    array( 'db' => 'channel_name', 'dt' => 2),
    array( 'db' => 'channel_image', 'dt' => 3 ),
    array( 'db' => 'id', 'dt' => 4)
);
$settings = parse_ini_file("classes/settings.ini.php");

$sql_details = array(
    'user' => $settings['user'],
    'pass' => $settings['password'],
    'db'   => $settings['dbname'],
    'host' => $settings['host']
);

require( 'ssp.class.php' );

echo json_encode(
    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null)
);
?>