<?php
include_once("header.php"); 
$categoryList = get_category();
// echo '<pre>';
// print_r($categoryList);
?>
<div class="container">
	<legend><strong><center>Channel</center></strong></legend>
	<div><button type="button" class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#addChannel">Add Channel <i class="fa fa-plus" aria-hidden="true"></i></button></div><br><br><br>

	<table id="channelDatatable" class="table table-striped table-hover responsive" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Category Name</th>
                <th>Channel Name</th>
                <th>Channel Image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Category Name</th>
                <th>Channel Name</th>
                <th>Channel Image</th>
                <th>Action</th>
            </tr>
        </tfoot>

        <tbody>

        </tbody>
    </table>
</div>
<div id="addChannel" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
        <form id="addChannelForm" class="form-horizontal" data-toggle="validator" role="form" novalidate="true" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title" id="modalTitle">
                    Add Channel
                </h4>
            </div>
            <div class="modal-body">
                
                <div class="form-group">
                    <label class="control-label col-sm-4">Channel ID : </label>
                    <div class="col-sm-6">
                        <input type="text" name="channelId" id="channelId" class="form-control" required value="">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-success"  onclick="callApi()">Call</button>
                    </div>
                    
                </div>
                <div id="displayContest" style="display: none;">
	                <div class="form-group">
	                    <label class="control-label col-sm-4">Select Category : </label>
	                    <div class="col-sm-6">
	                    	<div class="dropdown">
							  <select class="form-control" name="category_id" id="category_id" required>
							  	<option value="">Select Category</option>
							  	 <?php foreach ($categoryList as $row): ?>
							  	<option value="<?=$row["id"]?>"> <?=$row["category_name"]?></option>
							  	<?php endforeach ?>
							  </select>
							  
							</div>
	                        
	                        <div class="help-block with-errors"></div>
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class="control-label col-sm-4">Channel Name : </label>
	                    <div class="col-sm-6">
	                        <input type="text" name="channelName" id="channelName" class="form-control" required>
	                        <div class="help-block with-errors"></div>
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class="control-label col-sm-4">Channel Image : </label>
	                    <div class="col-sm-6">
	                        <img src="" id="img" class="img-rounded" alt="Cinque Terre" width="420" height="200">
	                    </div>
	                </div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>

  </div>
</div>
<div id="editChannel" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
        <form id="addChannelForm" class="form-horizontal" data-toggle="validator" role="form" novalidate="true" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title" id="modalTitle">
                    Edit Channel
                </h4>
            </div>
            <div class="modal-body">
                
                <div class="form-group">
                    <label class="control-label col-sm-4">Channel ID : </label>
                    <div class="col-sm-6">
                        <input type="text" name="editchannelId" id="editchannelId" class="form-control" required value="" disabled>
                        <div class="help-block with-errors"></div>
                    </div>
                    
                </div>
                <div id="displayContest">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Select Category : </label>
                        <div class="col-sm-6">
                            <div class="dropdown">
                              <select class="form-control" name="editcategory_id" id="editcategory_id" required>
                                <option value="">Select Category</option>
                                 <?php foreach ($categoryList as $row): ?>
                                <option value="<?=$row["id"]?>"> <?=$row["category_name"]?></option>
                                <?php endforeach ?>
                              </select>
                              
                            </div>
                            
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Channel Name : </label>
                        <div class="col-sm-6">
                            <input type="text" name="editchannelName" id="editchannelName" class="form-control" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Channel Image : </label>
                        <div class="col-sm-6">
                            <img src="" id="editImg" class="img-rounded" alt="Cinque Terre" width="420" height="200">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>

  </div>
</div>
<?php
include_once("js_files.php");  
?>
<script type="text/javascript">
$(document).ready(function() {
   var table = $('#channelDatatable').DataTable({
            "ajax": {
                "url": "fetch_channel.php",
                "processing": true,
                "serverSide": true,
            },  
            'columnDefs': [
            {
                'targets': 4,
                'searchable':false,
                'orderable':false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    return '<a href="javascript:editChannel('+data+')" class="btn btn-primary btn-flat"><i class="fa fa-pencil" title="Edit Category"></i></a> <a href="javascript:deleteChannel('+data+')" class="btn btn-danger btn-flat"><i class="fa fa-trash" title="Delete Category"></i></a>';
                }
            },
            {
                'targets': 4,
                'searchable':false,
                'orderable':false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
            
                    return '<img src="<?php echo IMG_URL?>'+data+'" width="50" height="50">';
                }
            }],
            'order': [1, 'asc'],
            "createdRow": function(row, data, dataIndex){
                $(row).attr("id", "tblRow_" + data[0]);
            }
        }); 
});
var channelData = [];
function callApi() 
{
	console.log($('#channelId').val());
	frm = new FormData(addChannelForm);
	frm.append('channelId',$('#channelId').val());
	frm.append('action','fetchChannelData');
	sa_ajax(frm, call_success, call_fail);
}
function call_success(res)
{
    if( res['status_code'] == 200 ) {
       
    	$("#displayContest").css('display','block');

    	var last = res['data']['items'].pop();
        // console.log(last.snippet.thumbnails.high.url);
        // console.log(res.data.items[2].snippet.thumbnails.high.url);
        $('#channelName').val(res.data.items[1].snippet.channelTitle);
        $('#img').attr('src',last.snippet.thumbnails.high.url);
    }
    if( res['status_code'] == 500 )
    {
        sa_message("error",res['message']);
    }
}
function call_fail(res)
{
	sa_message("error",res['message']);
}

$('#addChannelForm').validator().on('submit', function(e){
    if (!e.isDefaultPrevented()) {
        frm = new FormData(addChannelForm);
        frm.append("action","addChannel");
        frm.append("channel_image",$("#img").attr('src'));

        sa_ajax(frm, save_success, save_fail);
        e.preventDefault();
    }
});
function save_success(res)
{
	if(res['status_code'] == 200)
    {
        sa_message("success",res['message']);
        $("#addChannel").modal('hide');
        reset_form("addChannelForm");
        $("#channelDatatable").DataTable().ajax.reload();
    }

    if(res['status_code'] == 500) 
    {
        sa_message("error",res['message']);
    }
}
function save_fail(res)
{
    console.log(res)
}

function editChannel(id)
{
    console.log(id);
    frm = new FormData();
    frm.append('action','channelById');
    frm.append('channel_id',id);
    sa_ajax(frm, get_success, get_fail); 
    function get_success(res)
    {
        console.log(res);
        udata = res['data'][0];
        $("#editChannel").modal('show');
        $("#editchannelName").val(udata['channel_name']);
        $("#editImg").attr('src',udata['channel_image']);
        $("#editchannelId").val(udata['channel_id']);
        $('select[name="editcategory_id"]').find('option[value="'+udata['category_id']+'"]').attr("selected",true);
        //$("#editcategory_id option[value="+udata['category_id']+"]").val(udata['id']);
    }
    function get_fail(res)
    {
        console.log(res);
    }
}


function reset_form(form_id)
{
    document.getElementById(form_id).reset();
}
</script>