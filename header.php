<?php
include_once("css_files.php");

include_once("config.php");

if(!is_login())
{
    echo "<script>window.location='".BASE_URL."signin.php"."'</script>";
}
?>
<nav class="navbar navbar-inverse" style="border-radius: 0px;">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?=BASE_URL?>dashboard.php">Admin Panel</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
        	<li><a href="<?=BASE_URL?>category.php"><span class="glyphicon glyphicon-log-in"></span> Category</a></li>
        	<li><a href="<?=BASE_URL?>channel.php"><span class="glyphicon glyphicon-log-in"></span> Channel</a></li>
            <li><a href="<?=BASE_URL?>logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
    </div>
</nav>