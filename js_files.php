<!--new  -->
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.3/js/dataTables.select.min.js"></script>
<!-- end -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/validator.min.js"></script>

<script src="js/sweetalert.min.js"></script>

<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script type="text/javascript">
function sa_ajax(serialized_data, success_func, fail_func) 
{	
    $.ajax({
        url: '<?=BASE_URL?>classes/methods_api.php',
        data:serialized_data,
        method:"POST",
        processData:false,
        contentType:false,
        dataType:"json"
    })
    .done(function( data ) 
    {
        if(data.status_code==200)
        {
            success_func(data);
        }
        else
        {
            fail_func(data);
        }
    });
}


function sa_message(type,message)
{
    toastr[type](message);
}
</script>
