<?php
include_once("classes/Db.class.php");
include_once("classes/users.php");

$table = 'category';
$primaryKey = 'id';
$where = "status = 1";
$columns = array(
    array( 'db' => 'id', 'dt' => 0),
    array( 'db' => 'category_name',   'dt' => 1 ),
    array( 'db' => 'category_image', 'dt' => 2 ),
    array( 'db' => 'id', 'dt' => 3)
);

$settings = parse_ini_file("classes/settings.ini.php");

$sql_details = array(
    'user' => $settings['user'],
    'pass' => $settings['password'],
    'db'   => $settings['dbname'],
    'host' => $settings['host']
);

require( 'ssp.class.php' );

echo json_encode(
    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, $where )
);
?>