<?php include_once("css_files.php");
include_once("config.php"); 
if(is_login())
	$user_data = $_SESSION['session'];
if(is_login())
    {
    	echo "<script>window.location='".BASE_URL."'</script>";
    }?>
<div class = "container">
	<div class="wrapper">
		<form role="form" data-toggle="validator" id="loginForm" class="form-signin">       
			<h3 class="form-signin-heading">Welcome Back! Please Sign In</h3>
			<hr class="colorgraph"><br>
			<div class="form-group">
				<input type="email" name="email" id="email" class="form-control" placeholder="Email Address" required>
				<div class="help-block with-errors"></div>
			</div>

			<div class="form-group">
				<input type="password" data-minlength="6" name="password" id="password" class="form-control" placeholder="Password" required>
	            <div class="help-block with-errors"></div>   
            </div>		  

			<button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Login</button> 
			<input type="hidden" name="action" value="signin"> 			
		</form>			
	</div>
</div>
<?php include_once('js_files.php'); ?>
<script type="text/javascript">
	var frm = $('#loginForm');
	frm.submit(function (e) 
	{
		e.preventDefault();
		form = new FormData(loginForm);
		sa_ajax(form,success_login,error_login);
		function success_login(res)
		{
			console.log(res);
			if(res['status_code']==200)
			{
				sa_message("success",res['message']);
				setTimeout(function()
				{
					window.location="<?=BASE_URL?>dashboard.php";
				},1000);
			}

			if(res['status_code']==404)	
			{
				sa_message("error",res['message']);
			}

		}
		function error_login(err)
		{
			console.log(err);
		}
	});
</script>